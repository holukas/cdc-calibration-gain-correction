Script to correct measured CO2 concentrations in eddy covariance raw data files by applying a gain value.

For more info see here:
https://www.swissfluxnet.ethz.ch/index.php/knowledge-base/wrong-calibration-gas-2017/