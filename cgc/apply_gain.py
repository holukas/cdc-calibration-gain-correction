"""

APPLY GAIN TO CO2 MEASUREMENTS
------------------------------

This script was developed to apply gain to the CO2 eddy covariance
raw data measurements (20Hz) in certain years. CO2 was wrong due to
the use of a wrong calibration gas.


version: 0.4
date: 25 Apr 2020

- v0.4: plotting adjustments
- v0.3: added .fillna()

Used for the following sites:
- CH-AWS 2017-2019

"""

# Columns to apply gain to
ppm_col = 'CO2_ppm_72'
mmol_col = 'CO2_mmol_m-3_72'
gain = 0.974
plots = False

import fnmatch
import os
from pathlib import Path

import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Dirs
dir_root = os.path.dirname(os.path.abspath(__file__))
dir_in = Path(dir_root) / 'converted_raw_data_csv'
dir_out = Path(dir_root) / 'converted_raw_data_csv_GAIN'

# Search files and store filenames and their location in dict
found_files_dict = {}
for root, dirs, files in os.walk(dir_in):
    for idx, filename in enumerate(files):
        if fnmatch.fnmatch(filename, '*.csv'):
            filepath = Path(root) / filename
            found_files_dict[filename] = filepath

# File loop
num_files = len(found_files_dict)
file_counter = 0
for filename, filepath in found_files_dict.items():
    file_counter += 1
    perc = (file_counter / num_files) * 100
    print(f"File {file_counter} of {num_files} ({perc:.1f}%) - {filename}")

    data_df = pd.read_csv(filepath,
                          skiprows=[],
                          header=[0],
                          na_values=-9999,
                          encoding='utf-8',
                          delimiter=',',
                          mangle_dupe_cols=True,
                          keep_date_col=False,
                          parse_dates=False,
                          # date_parser=parse,
                          index_col=None,
                          dtype=None,
                          nrows=None)  # nrows for testing

    # Gain application
    data_df[f'{ppm_col}_GAIN-{gain}'] = data_df[ppm_col].multiply(gain)
    data_df[f'{mmol_col}_GAIN-{gain}'] = data_df[mmol_col].multiply(gain)

    # Fill missing
    data_df.fillna(-9999, inplace=True)

    # Save file
    out_filepath = dir_out / filename
    data_df.to_csv(out_filepath, index=False)

    if plots:
        # Plot
        fig = plt.Figure()
        gs = gridspec.GridSpec(2, 1)  # rows, cols
        gs.update(wspace=0.3, hspace=0.3, left=0.03, right=0.97, top=0.97, bottom=0.03)
        ax_ppm = fig.add_subplot(gs[0, 0])
        ax_mmol = fig.add_subplot(gs[1, 0])

        plot_data_df = data_df.copy().replace(-9999, np.nan)
        for d in plot_data_df.columns:
            if ppm_col in d:
                ax_ppm.plot(plot_data_df.index, plot_data_df[d], label=d)
            elif mmol_col in d:
                ax_mmol.plot(plot_data_df.index, plot_data_df[d], label=d)

        ax_ppm.legend(loc='lower right')
        ax_mmol.legend(loc='lower right')

        out_filepath = dir_out / f'{filename}.png'
        fig.savefig(out_filepath, format='png', bbox_inches='tight', facecolor='w',
                    transparent=True, dpi=72)

print("------------------------------")
print("OK THIS IS THE END OF THE CODE")
